# PC Builder Sim

PC Builder simulator for MTI Rock Your School.

## Requirements
* Python 3
* Modules:
    * Pillow
    * pypiwin32
    * win32gui
    * provoli


## Programs to use
IDE:
* Visual Studio Code ([download portable zip](https://code.visualstudio.com/docs/?dv=winzip))

Python:
* WinPython 3.7.1 ([download portable installer](https://marionstudents-my.sharepoint.com/:u:/g/personal/christian_sirolli_marionstudents_net/EWrtJpjxKDtArLD30QvfuuMBNbeUgRkVhMfJ5eNHg0H7qQ?e=qe4hrR))