# -*- mode: python -*-

block_cipher = None


a = Analysis(['sim.py'],
             pathex=['C:\\Users\\christian.sirolli\\OneDrive - Marion County School District\\Documents\\Scripts\\Python\\pc-builder-sim', 'C:\\Users\christian.sirolli\\OneDrive - Marion County School District\\Documents\\Scripts\\Python'],
             binaries=[],
             datas=[('README.md', '.'), ('LICENSE', '.'), ('desktop_solid.ico', '.'), ('img/Block Party 14 - 16x9.jpg', 'img'), ('img/mti-logo.jpg', 'img'), ('img/Bright Future Blank - 16x9.jpg', 'img'), ('img/Broken Shine 08 - 16x9.jpg', 'img'), ('img/close.gif', 'img'), ('img/cpu.png', 'img'), ('img/cpu_in.png', 'img'), ('img/desktop_solid.png', 'img'), ('img/Forest Shine 06 - 16x9.jpg', 'img'), ('img/pc_empty.png', 'img'), ('img/pc_full.png', 'img'), ('img/gpu.png', 'img'), ('img/gpu_in.png', 'img'), ('img/hdd.png', 'img'), ('img/hdd_in.png', 'img'), ('img/LED Blitz Zone - 16x9.jpg', 'img'), ('img/Lit LED All Star - 16x9.jpg', 'img'), ('img/Marble Flow Warped 05 - 16x9.jpg', 'img'), ('img/minimize.gif', 'img'), ('img/Neon Offset 07 - 16x9.jpg', 'img'), ('img/Noise Play Hero - 16x9.jpg', 'img'), ('img/Northern Pine Warm Redwood - 16x9.jpg', 'img'), ('img/odd.png', 'img'), ('img/odd_in.png', 'img'), ('img/psu.png', 'img'), ('img/psu_in.png', 'img'), ('img/Rail Glitch 06 - 16x9.jpg', 'img'), ('img/ram.png', 'img'), ('img/ram_in.png', 'img'), ('img/Rock Flag USA - 16x9.jpg', 'img'), ('img/Smooth Candy 09 - 16x9.jpg', 'img'), ('img/Split Smoke Roar - 16x9.jpg', 'img'), ('img/Stained Glass Ocean - 16x9.jpg', 'img'), ('img/Summer Splash Festival - 16x9.jpg', 'img'), ('img/zoom.gif', 'img'), ('img/zoom_alt.gif', 'img')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='sim',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False, icon='desktop_solid.ico')
