REM pyinstaller must be installed (pip install pyinstaller) and the Python Scripts folder ought to be in the PATH variable
pyinstaller --noconfirm ^
    -F -w ^
    --add-data="README.md;." --add-data="LICENSE;." --add-data="desktop_solid.ico;." ^
	--add-data="img/Block Party 14 - 16x9.jpg;img" --add-data="img/Bright Future Blank - 16x9.jpg;img" --add-data="img/Broken Shine 08 - 16x9.jpg;img" --add-data="img/close.gif;img" --add-data="img/cpu.png;img" ^
	--add-data="img/cpu_in.png;img" --add-data="img/desktop_solid.png;img" --add-data="img/Forest Shine 06 - 16x9.jpg;img" --add-data="img/pc_empty.png;img" --add-data="img/pc_full.png;img" ^
	--add-data="img/gpu.png;img" --add-data="img/gpu_in.png;img" --add-data="img/hdd.png;img" --add-data="img/hdd_in.png;img" --add-data="img/LED Blitz Zone - 16x9.jpg;img" ^
	--add-data="img/Lit LED All Star - 16x9.jpg;img" --add-data="img/Marble Flow Warped 05 - 16x9.jpg;img" --add-data="img/minimize.gif;img" --add-data="img/Neon Offset 07 - 16x9.jpg;img" ^
	--add-data="img/Noise Play Hero - 16x9.jpg;img" --add-data="img/Northern Pine Warm Redwood - 16x9.jpg;img" --add-data="img/odd.png;img" --add-data="img/odd_in.png;img" --add-data="img/psu.png;img" ^
	--add-data="img/psu_in.png;img" --add-data="img/Rail Glitch 06 - 16x9.jpg;img" --add-data="img/ram.png;img" --add-data="img/ram_in.png;img" --add-data="img/Rock Flag USA - 16x9.jpg;img" ^
	--add-data="img/Smooth Candy 09 - 16x9.jpg;img" --add-data="img/Split Smoke Roar - 16x9.jpg;img" --add-data="img/Stained Glass Ocean - 16x9.jpg;img" --add-data="img/Summer Splash Festival - 16x9.jpg;img" ^
	--add-data="img/zoom.gif;img" --add-data="img/zoom_alt.gif;img" --add-data="img/mti-logo.jpg;img" ^
	-i=desktop_solid.ico ^
    sim.py