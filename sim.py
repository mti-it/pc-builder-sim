# pip install Pillow
import tkinter as tk
from PIL import Image, ImageTk  # pip install Pillow
from ctypes import windll, Structure, c_long, byref
from win32con import SW_MINIMIZE
import time
from threading import Timer, RLock
import random
import math
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)), os.pardir)))
sys.path.insert(0, 'C:/Users/christian.sirolli/Documents/Scripts/Python/TDT/provoli')
import provoli
GWL_EXSTYLE = -20
WS_EX_APPWINDOW = 0x00040000
WS_EX_TOOLWINDOW = 0x00000080

root = tk.Tk()


class POINT(Structure):
    _fields_ = [("x", c_long), ("y", c_long)]


def queryMousePosition():
    pt = POINT()
    windll.user32.GetCursorPos(byref(pt))
    return (pt.x, pt.y)


class Item:
    def __init__(self, name, fname, root, app, master, dest, x, pos, reset, get_correct, set_correct, pc_pic_bbox=None, width=None, height=None):
        self.master = master
        self.name = name
        self.dest = dest
        self.root = root
        self.app = app
        self.fname = fname
        self.x_original = x
        self.drag_start_x = self.drag_start_y = 0
        self.canvas = tk.Canvas(self.master, bd=1, bg="gray",
                                width=120, height=120)
        self.app.update()
        self.image = ImageTk.PhotoImage(self.root.images[self.name])
        self.canvas.create_image(60, 3, anchor=tk.N, image=self.image)
        self.label = tk.Label(self.canvas, text=self.fname,
                              fg='black', bg='white')
        self.label.pack()
        self.canvas.create_window(
            60, 110, width=120, height=20, window=self.label)
        self.canvas.bind("<Button-1>", self.drag_start)
        self.canvas.bind("<B1-Motion>", self.drag_motion)
        self.canvas.bind("<ButtonRelease>", self.drag_release)
        self.x = self.x_original
        self.y = self.app.winfo_height()
        self.pos = pos
        if self.name == 'RAM':
            self.pos_alt = (pc_pic_bbox[0] + (41 / 100 * width), pc_pic_bbox[1] + (35.55 / 100 * height),
                            pc_pic_bbox[0] + (58.3 / 100 * width), pc_pic_bbox[1] + (72.75 / 100 * height))
        else:
            self.pos_alt = pos
        self.reset_cmd = reset
        self.get_correct = get_correct
        self.set_correct = set_correct

    def drag_start(self, event):
        self.app.update()
        self.drag_start_x = event.x
        self.drag_start_y = event.y

    def drag_motion(self, event):
        self.x = self.canvas.winfo_x() - self.drag_start_x + event.x
        self.y = self.canvas.winfo_y() - self.drag_start_y + event.y + 120
        self.canvas.place(x=self.x, y=self.y)

    def drag_release(self, event):
        if self.pos_alt[0] <= self.x <= self.pos_alt[2] and self.pos_alt[1] <= self.y <= self.pos_alt[3]:
            self.canvas.place(x=self.x, y=self.y)
            self.master.config(bg="green")
            self.dest.config(bg="green")
            self.canvas.place_forget()
            self.image_in = ImageTk.PhotoImage(self.root.images[self.name+'_in'].resize(
                (int(self.pos[2]-self.pos[0]), int(self.pos[3]-self.pos[1]))))
            self.dest.create_image(
                self.pos[0], self.pos[1], anchor=tk.NW, image=self.image_in)
            self.set_correct(self.get_correct()+1)
            t = Timer(0.25, self.reset)
            t.start()
        else:
            self.canvas.place(x=self.x_original, y=self.app.winfo_height())
            self.master.config(bg="red")
            self.dest.config(bg="red")
            t = Timer(0.25, self.reset)
            t.start()

    def reset(self):
        self.master.config(bg="white")
        self.dest.config(bg="black")


class Sim():
    def __init__(self):
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            self.base_path = sys._MEIPASS
        except Exception:
            self.base_path = os.path.abspath(".")
        self.root = provoli.App(root, title="PC Builder Sim - IT Academy - MTI Rock Your School", icon=self.base_path+'\\desktop_solid.ico',
                                min_icon=self.base_path+"\\img\\minimize.gif", zoom_icon=self.base_path+"\\img\\zoom.gif", zoom_alt_icon=self.base_path+"\\img\\zoom_alt.gif",
                                close_icon=self.base_path+"\\img\\close.gif", bg='black', titlebg='lightgray', titlefg='black', state='zoomed', zoom=False, minimize=False, i_height=800, i_width=800)
        self.images = {
            "Computer_Empty": Image.open(self.base_path+"\\img\\pc_empty.png").resize((int((self.root.winfo_height()-100)*1.17806267806), self.root.winfo_height()-100)),
            "Computer_Full": Image.open(self.base_path+"\\img\\pc_full.png").resize((int((self.root.winfo_height()-100)*1.17806267806), self.root.winfo_height()-100)),
            "CPU": Image.open(self.base_path+"\\img\\cpu.png").resize((100, 100), Image.ANTIALIAS),
            "CPU_in": Image.open(self.base_path+"\\img\\cpu_in.png"),
            "RAM": Image.open(self.base_path+"\\img\\ram.png").resize((100, 100), Image.ANTIALIAS),
            "RAM_in": Image.open(self.base_path+"\\img\\ram_in.png"),
            "GPU": Image.open(self.base_path+"\\img\\gpu.png").resize((100, 100), Image.ANTIALIAS),
            "GPU_in": Image.open(self.base_path+"\\img\\gpu_in.png"),
            "PSU": Image.open(self.base_path+"\\img\\psu.png").resize((100, 100), Image.ANTIALIAS),
            "PSU_in": Image.open(self.base_path+"\\img\\psu_in.png"),
            "HDD": Image.open(self.base_path+"\\img\\hdd.png").resize((100, 100), Image.ANTIALIAS),
            "HDD_in": Image.open(self.base_path+"\\img\\hdd_in.png"),
            "ODD": Image.open(self.base_path+"\\img\\odd.png").resize((100, 100), Image.ANTIALIAS),
            "ODD_in": Image.open(self.base_path+"\\img\\odd_in.png")
        }
        self.bgs = [
            # backgrounds from CMG
            self.base_path+"\\img\\Block Party 14 - 16x9.jpg",
            self.base_path+"\\img\\Bright Future Blank - 16x9.jpg",
            self.base_path+"\\img\\Broken Shine 08 - 16x9.jpg",
            self.base_path+"\\img\\Forest Shine 06 - 16x9.jpg",
            self.base_path+"\\img\\LED Blitz Zone - 16x9.jpg",
            self.base_path+"\\img\\Lit LED All Star - 16x9.jpg",
            self.base_path+"\\img\\Marble Flow Warped 05 - 16x9.jpg",
            self.base_path+"\\img\\Neon Offset 07 - 16x9.jpg",
            self.base_path+"\\img\\Noise Play Hero - 16x9.jpg",
            self.base_path+"\\img\\Northern Pine Warm Redwood - 16x9.jpg",
            self.base_path+"\\img\\Rail Glitch 06 - 16x9.jpg",
            self.base_path+"\\img\\Rock Flag USA - 16x9.jpg",
            self.base_path+"\\img\\Smooth Candy 09 - 16x9.jpg",
            self.base_path+"\\img\\Split Smoke Roar - 16x9.jpg",
            self.base_path+"\\img\\Stained Glass Ocean - 16x9.jpg",
            self.base_path+"\\img\\Summer Splash Festival - 16x9.jpg"
        ]
        self.academies = {
            "Automotive": [],
            "Business Finance": [],
            "Construction": [],
            "Culinary Arts": [],
            "Early Childhood Education": [],
            "Integrated First Responders": [],
            "Information Technology": []
        }
        self.fontsize = 16
        self.set_correct(0)
        self.set_bindings()
        self.menu()

        root.mainloop()
        self.root.update = self.update

    def menu(self):
        '''
        This is the menu screen for the game.
        Whenever a player selects their class and clicks play, the game will begin
        '''
        self.stage = 'menu'
        self.frame = tk.Frame(self.root.content, width=self.root.winfo_width(
        ), height=self.root.winfo_height(), bg='white')
        self.frame.grid_columnconfigure(0, weight=2)
        self.frame.grid_rowconfigure(0, weight=2)
        self.frame.grid_rowconfigure(1, weight=2)
        self.frame.grid_rowconfigure(2, weight=2)

        background = random.choice(self.bgs)
        self.background_image = ImageTk.PhotoImage(Image.open(background))
        self.background_label = tk.Label(
            self.frame, image=self.background_image)
        self.background_label.image = self.background_image

        self.frame2 = tk.Frame(
            self.frame, width=self.root.winfo_width(), height=self.root.winfo_height())
        self.root.root.update()

        self.label1 = tk.Label(self.frame2, text="Which academy are you in?", font=(
            "Helvetica", self.fontsize), width=55, height=1)
        self.button1 = tk.Button(self.frame2, text="Automotive", command=lambda: self.startGame(
            academy='Automotive'), font=("Helvetica", self.fontsize), width=25, height=1)
        self.button2 = tk.Button(self.frame2, text="Business Finance", command=lambda: self.startGame(
            academy='Business Finance'), font=("Helvetica", self.fontsize), width=25, height=1)
        self.button3 = tk.Button(self.frame2, text="Construction", command=lambda: self.startGame(
            academy='Construction'), font=("Helvetica", self.fontsize), width=25, height=1)
        self.button4 = tk.Button(self.frame2, text="Culinary Arts", command=lambda: self.startGame(
            academy='Culinary Arts'), font=("Helvetica", self.fontsize), width=25, height=1)
        self.button5 = tk.Button(self.frame2, text="Early Childhood Education", command=lambda: self.startGame(
            academy='Early Childhood Education'), font=("Helvetica", self.fontsize), width=25, height=1)
        self.button6 = tk.Button(self.frame2, text="Integrated First Responders", command=lambda: self.startGame(
            academy='Integrated First Responders'), font=("Helvetica", self.fontsize), width=25, height=1)
        self.button7 = tk.Button(self.frame2, text="Information Technology", command=lambda: self.startGame(
            academy='Information Technology'), font=("Helvetica", self.fontsize), width=25, height=1)

        self.logo = self.base_path+"\\img\\mti-logo.jpg"
        self.logoImg = ImageTk.PhotoImage(Image.open(self.logo))
        self.logoLabel = tk.Label(
            self.frame, image=self.logoImg, width=211, height=170)

        self.frame3 = tk.Frame(self.frame)
        self.root.root.update()
        self.label2 = tk.Label(self.frame3, text="High Scores", font=(
            "Helvetica", self.fontsize), width=35, height=1)
        self.label3 = tk.Label(self.frame3, anchor=tk.W, text="Automotive:", font=(
            "Helvetica", self.fontsize), width=25//2+20, height=1)
        self.label4 = tk.Label(self.frame3, anchor=tk.E, text="{}".format('—' if self.academies['Automotive'] == [] else '{0:.1f}s'.format(
            float(min(self.academies['Automotive'])))), font=("Helvetica", self.fontsize), width=25//2-20, height=1)
        self.label5 = tk.Label(self.frame3, anchor=tk.W, text="Business Finance:", font=(
            "Helvetica", self.fontsize), width=25//2+20, height=1)
        self.label6 = tk.Label(self.frame3, anchor=tk.E, text="{}".format('—' if self.academies['Business Finance'] == [] else '{0:.1f}s'.format(
            float(min(self.academies['Business Finance'])))), font=("Helvetica", self.fontsize), width=25//2-20, height=1)
        self.label7 = tk.Label(self.frame3, anchor=tk.W, text="Construction:", font=(
            "Helvetica", self.fontsize), width=25//2+20, height=1)
        self.label8 = tk.Label(self.frame3, anchor=tk.E, text="{}".format('—' if self.academies['Construction'] == [] else '{0:.1f}s'.format(
            float(min(self.academies['Construction'])))), font=("Helvetica", self.fontsize), width=25//2-20, height=1)
        self.label9 = tk.Label(self.frame3, anchor=tk.W, text="Culinary Arts:", font=(
            "Helvetica", self.fontsize), width=25//2+20, height=1)
        self.label10 = tk.Label(self.frame3, anchor=tk.E, text="{}".format('—' if self.academies['Culinary Arts'] == [] else '{0:.1f}s'.format(
            float(min(self.academies['Culinary Arts'])))), font=("Helvetica", self.fontsize), width=25//2-20, height=1)
        self.label11 = tk.Label(self.frame3, anchor=tk.W, text="Early Childhood Education:", font=(
            "Helvetica", self.fontsize), width=25//2+20, height=1)
        self.label12 = tk.Label(self.frame3, anchor=tk.E, text="{}".format('—' if self.academies['Early Childhood Education'] == [] else '{0:.1f}s'.format(
            float(min(self.academies['Early Childhood Education'])))), font=("Helvetica", self.fontsize), width=25//2-20, height=1)
        self.label13 = tk.Label(self.frame3, anchor=tk.W, text="Integrated First Responders:", font=(
            "Helvetica", self.fontsize), width=25//2+20, height=1)
        self.label14 = tk.Label(self.frame3, anchor=tk.E, text="{}".format('—' if self.academies['Integrated First Responders'] == [] else '{0:.1f}s'.format(
            float(min(self.academies['Integrated First Responders'])))), font=("Helvetica", self.fontsize), width=25//2-20, height=1)
        self.label15 = tk.Label(self.frame3, anchor=tk.W, text="Information Technology:", font=(
            "Helvetica", self.fontsize), width=25//2+20, height=1)
        self.label16 = tk.Label(self.frame3, anchor=tk.E, text="{}".format('—' if self.academies['Information Technology'] == [] else '{0:.1f}s'.format(
            float(min(self.academies['Information Technology'])))), font=("Helvetica", self.fontsize), width=25//2-20, height=1)
        self.update()

    def startGame(self, academy="Information Technology"):
        self.stage = 'startGame'
        self.academy = academy
        self.frame4 = tk.Frame(self.frame, bg='lightgray', width=self.frame.winfo_width()//3, height=self.frame.winfo_height() //
                               3, bd=1, relief=tk.RAISED, highlightbackground="red", highlightcolor="red", highlightthickness=1)
        self.label17 = tk.Label(self.frame4, text='Did you select {}?'.format(
            self.academy), font=("Helvetica", self.fontsize))
        self.button8 = tk.Button(self.frame4, text='Yes', command=lambda: self.game(
        ), font=("Helvetica", self.fontsize))
        self.button9 = tk.Button(self.frame4, text='No', command=lambda: self.restart(
        ), font=("Helvetica", self.fontsize))
        self.update()

    def game(self):
        self.stage = 'game'
        self.start = time.time()
        self.frame.pack_forget()
        self.root.update()
        self.set_correct(0)
        self.frame = tk.Frame(self.root.content, width=self.root.winfo_width(
        ), height=self.root.winfo_height(), bg="white")
        self.frame.pack(fill=tk.BOTH, expand=tk.YES)
        # Computer Case
        self.root.update()
        empty_pc = ImageTk.PhotoImage(image=self.images["Computer_Empty"])
        self.canvas = tk.Canvas(self.frame, bg="black", bd=0, width=self.root.winfo_width(
        ), height=self.root.winfo_height()-120)
        self.canvas.place(y=0, x=0, bordermode="outside", width=self.root.content.winfo_width(
        ), height=self.root.winfo_height()-120, anchor=tk.NW)
        self.root.update()
        pc_pic = self.canvas.create_image(self.canvas.winfo_width(
        ) // 2, (self.canvas.winfo_height()) // 2, image=empty_pc)
        self.time = 'Time: {0:.1f}s'.format(float(time.time() - self.start))
        self.timeLabel = tk.Label(
            self.canvas, bg="white", textvariable=self.time)
        self.timeLabel.place(x=0, y=0, anchor=tk.NW)
        self.canvas.create_window(
            0, 0, window=self.timeLabel, anchor=tk.NW, height=30, width=100)
        # returns a tuple like (x1, y1, x2, y2)
        pc_pic_bbox = self.canvas.bbox(pc_pic)
        self.rect = {}
        w = 0
        width = (pc_pic_bbox[2] - pc_pic_bbox[0])
        height = (pc_pic_bbox[3] - pc_pic_bbox[1])
        # left x position,                            top y position
        # right x position,                            bottom y position
        self.rect["psu"] = self.canvas.create_rectangle(pc_pic_bbox[0] + (2.30 / 100 * width),    pc_pic_bbox[1],
                                                        pc_pic_bbox[0] + (47.8 / 100 * width),    pc_pic_bbox[1] + (35.8 / 100 * height), width=w, outline="purple")  # PSU
        self.rect["odd"] = self.canvas.create_rectangle(pc_pic_bbox[2] - (48.85 / 100 * width),    pc_pic_bbox[1] + (1.85 / 100 * width),
                                                        pc_pic_bbox[2] - (3.25 / 100 * width),        pc_pic_bbox[1] + (35.8 / 100 * height), width=w, outline="orange")  # ODD
        self.rect["cpu"] = self.canvas.create_rectangle(pc_pic_bbox[0] + (24.7 / 100 * width),    pc_pic_bbox[1] + (39.7 / 100 * height),
                                                        pc_pic_bbox[0] + (43.9 / 100 * width),    pc_pic_bbox[1] + (67.2 / 100 * height), width=w, outline="blue")  # CPU
        self.rect["ram"] = self.canvas.create_rectangle(pc_pic_bbox[0] + (43.6 / 100 * width),    pc_pic_bbox[1] + (35.55 / 100 * height),
                                                        pc_pic_bbox[0] + (58.3 / 100 * width),    pc_pic_bbox[1] + (72.75 / 100 * height), width=w, outline="green")  # RAM
        self.rect["gpu"] = self.canvas.create_rectangle(pc_pic_bbox[0] + (0.0 / 100 * width),    pc_pic_bbox[3] - (32.25 / 100 * height),
                                                        pc_pic_bbox[0] + (43.5 / 100 * width),    pc_pic_bbox[3] - (0.2052 / 100 * height), width=w, outline="yellow")  # GPU
        self.rect["hdd"] = self.canvas.create_rectangle(pc_pic_bbox[2] - (43.55 / 100 * width),    pc_pic_bbox[3] - (36.5 / 100 * height),
                                                        pc_pic_bbox[2] - (1.35 / 100 * width),    pc_pic_bbox[3] - math.ceil(0.5 / 100 * height), width=w, outline="red")  # HDD
        self.canvas.pc_empty = empty_pc
        # Part List
        # CPU
        self.option1 = Item(name="CPU", fname="CPU", root=self, app=self.root, master=self.frame, dest=self.canvas, x=(
            self.root.winfo_width()/6*1)-206, pos=self.canvas.bbox(self.rect["cpu"]), reset=self.menu, get_correct=self.get_correct, set_correct=self.set_correct)
        # RAM
        self.option2 = Item(name="RAM", fname="RAM", root=self, app=self.root, master=self.frame, pc_pic_bbox=pc_pic_bbox, width=width, height=height, dest=self.canvas, x=(
            self.root.winfo_width()/6*2)-206, pos=self.canvas.bbox(self.rect["ram"]), reset=self.menu, get_correct=self.get_correct, set_correct=self.set_correct)
        # GPU
        self.option3 = Item(name="GPU", fname="Graphics Card", root=self, app=self.root, master=self.frame, dest=self.canvas, x=(
            self.root.winfo_width()/6*3)-206, pos=self.canvas.bbox(self.rect["gpu"]), reset=self.menu, get_correct=self.get_correct, set_correct=self.set_correct)
        # PSU
        self.option4 = Item(name="PSU", fname="Power Supply", root=self, app=self.root, master=self.frame, dest=self.canvas, x=(
            self.root.winfo_width()/6*4)-206, pos=self.canvas.bbox(self.rect["psu"]), reset=self.menu, get_correct=self.get_correct, set_correct=self.set_correct)
        # HDD
        self.option5 = Item(name="HDD", fname="Hard Drive", root=self, app=self.root, master=self.frame, dest=self.canvas, x=(
            self.root.winfo_width()/6*5)-206, pos=self.canvas.bbox(self.rect["hdd"]), reset=self.menu, get_correct=self.get_correct, set_correct=self.set_correct)
        # ODD
        self.option6 = Item(name="ODD", fname="DVD Drive", root=self, app=self.root, master=self.frame, dest=self.canvas, x=(
            self.root.winfo_width()/6*6)-206, pos=self.canvas.bbox(self.rect["odd"]), reset=self.menu, get_correct=self.get_correct, set_correct=self.set_correct)
        self.update()

    def restart(self, event=None):
        self.frame.pack_forget()
        self.menu()

    def partRestart(self, event=None):
        self.frame.pack_forget()
        self.game()

    def get_correct(self):
        return self.correct

    def set_correct(self, value):
        self.correct = value
        if self.get_correct() == 6:
            self.end_game()

    def end_game(self, event=None):
        self.stage = 'end_game'
        self.end = time.time()
        self.academies[self.academy].append(self.end - self.start)
        self.doneFrame = tk.Frame(self.frame, width=self.frame.winfo_width(
        )//2, height=self.frame.winfo_height()//3, bg="green")
        self.doneFrame.place(x=self.frame.winfo_width()//2, y=self.frame.winfo_height()//2,
                             width=self.frame.winfo_width()//2, height=self.frame.winfo_height()//3, anchor=tk.CENTER)
        self.root.update()

        self.pc_full = ImageTk.PhotoImage(image=self.images["Computer_Full"])
        self.pc_pic = self.canvas.create_image(self.canvas.winfo_width(
        ) // 2, (self.canvas.winfo_height()) // 2, image=self.pc_full)
        self.doneLabel = tk.Label(self.doneFrame, text="Congrats! You won the game!\nNow get to another station, quick!",
                                  width=self.doneFrame.winfo_width(), height=50, bg="green", fg="white", font=("Helvetica", self.fontsize))

        self.doneButton = tk.Button(self.doneFrame, text="RESET", width=self.doneFrame.winfo_width(
        )-50, height=50, bg="white", fg="black", command=self.restart, font=("Helvetica", self.fontsize))
        self.doneScore = tk.Label(self.doneFrame, text='Time: {0:.1f}s'.format(float(
            self.end - self.start)), bg="green", fg="white", font=("Helvetica", self.fontsize))
        self.update()

    def set_bindings(self):
        root.bind("<Control-r>", self.restart)
        root.bind("<Control-Alt-r>", self.partRestart)
        root.bind("<Control-Alt-d>", self.end_game)
        self.root.title_bar.bind("<ButtonRelease-1>", self.StopMove)
        self.root.top.bind("<ButtonRelease-1>", self.StopMove)

    def StopMove(self, event):
        cx, cy = queryMousePosition()
        if cy <= 0:
            self.root.zoom()
        elif cx <= 0:
            self.root.snap_left()
        elif cx >= self.root.work_area[2] - 1:
            self.root.snap_right()
        self.root._offsetx = 0
        self.root._offsety = 0
        self.root.x = self.root.root.winfo_rootx()
        self.root.y = self.root.root.winfo_rooty()
        self.update()

    def update(self):
        self.root.root.update()
        self.root.root.update_idletasks()
        self.root.r_width = self.root.winfo_width()
        self.root.r_height = self.root.winfo_height()

        self.root.root_frame.pack(fill=tk.BOTH, expand=1)
        self.root.top.grid(row=0, column=0)
        self.root.content.grid(row=1, column=0)
        self.root.__icon__.pack(side=tk.LEFT, anchor=tk.N, padx=5)
        self.root.closeBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=10)
        self.root.zoomBtn.pack(side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=10)
        self.root.minimizeBtn.pack(
            side=tk.RIGHT, anchor=tk.N, ipady=5, ipadx=10)
        self.root.title_bar.pack(
            side=tk.LEFT, fill=tk.X, anchor=tk.N, padx=5, pady=5)
        if self.root.state == 'unzoomed':
            self.root.root.x = self.root.root.winfo_rootx()
            self.root.root.y = self.root.root.winfo_rooty()
        self.root.root.update()

        if self.stage == 'menu':
            self.frame.config(width=self.root.winfo_width(),
                              height=self.root.winfo_height())
            self.frame.place(x=0, y=0, relwidth=1, relheight=1)
            self.background_label.place(x=0, y=0, relwidth=1, relheight=1)
            self.frame2.config(width=self.root.winfo_width(),
                               height=self.root.winfo_height())
            self.frame2.grid(row=0, column=0, sticky=tk.N, pady=(5, 0))
            self.label1.grid(column=0, row=0, columnspan=2,
                             sticky=tk.N, padx=1, pady=5)
            self.button1.grid(column=0, row=1, sticky=tk.N, padx=1, pady=5)
            self.button2.grid(column=0, row=2, sticky=tk.N, padx=1, pady=5)
            self.button3.grid(column=0, row=3, sticky=tk.N, padx=1, pady=5)
            self.button4.grid(column=1, row=1, sticky=tk.N, padx=1, pady=5)
            self.button5.grid(column=1, row=2, sticky=tk.N, padx=1, pady=5)
            self.button6.grid(column=1, row=3, sticky=tk.N, padx=1, pady=5)
            self.button7.grid(column=0, row=4, columnspan=2,
                              sticky=tk.N, padx=1, pady=5)
            self.logoLabel.grid(row=1, column=0)
            self.frame3.grid(row=2, column=0, sticky=tk.S, pady=(0, 10))
            self.label2.grid(row=0, column=0, columnspan=2, pady=2)
            self.label3.grid(row=1, column=0, pady=2)
            self.label4.grid(row=1, column=1, pady=2)
            self.label5.grid(row=2, column=0, pady=2)
            self.label6.grid(row=2, column=1, pady=2)
            self.label7.grid(row=3, column=0, pady=2)
            self.label8.grid(row=3, column=1, pady=2)
            self.label9.grid(row=4, column=0, pady=2)
            self.label10.grid(row=4, column=1, pady=2)
            self.label11.grid(row=5, column=0, pady=2)
            self.label12.grid(row=5, column=1, pady=2)
            self.label13.grid(row=6, column=0, pady=2)
            self.label14.grid(row=6, column=1, pady=2)
            self.label15.grid(row=7, column=0, pady=2)
            self.label16.grid(row=7, column=1, pady=2)
        elif self.stage == 'startGame':
            self.frame.place(x=0, y=0, relwidth=1, relheight=1)
            self.background_label.place(x=0, y=0, relwidth=1, relheight=1)
            self.frame2.grid(row=0, column=0, sticky=tk.N, pady=(5, 0))
            self.label1.grid(column=0, row=0, columnspan=2,
                             sticky=tk.N, padx=1, pady=5)
            self.button1.grid(column=0, row=1, sticky=tk.N, padx=1, pady=5)
            self.button2.grid(column=0, row=2, sticky=tk.N, padx=1, pady=5)
            self.button3.grid(column=0, row=3, sticky=tk.N, padx=1, pady=5)
            self.button4.grid(column=1, row=1, sticky=tk.N, padx=1, pady=5)
            self.button5.grid(column=1, row=2, sticky=tk.N, padx=1, pady=5)
            self.button6.grid(column=1, row=3, sticky=tk.N, padx=1, pady=5)
            self.button7.grid(column=0, row=4, columnspan=2,
                              sticky=tk.N, padx=1, pady=5)
            self.logoLabel.grid(row=1, column=0)
            self.frame3.grid(row=2, column=0, sticky=tk.S, pady=(0, 5))
            self.label2.grid(row=0, column=0, columnspan=2, pady=2)
            self.label3.grid(row=1, column=0, pady=2)
            self.label4.grid(row=1, column=1, pady=2)
            self.label5.grid(row=2, column=0, pady=2)
            self.label6.grid(row=2, column=1, pady=2)
            self.label7.grid(row=3, column=0, pady=2)
            self.label8.grid(row=3, column=1, pady=2)
            self.label9.grid(row=4, column=0, pady=2)
            self.label10.grid(row=4, column=1, pady=2)
            self.label11.grid(row=5, column=0, pady=2)
            self.label12.grid(row=5, column=1, pady=2)
            self.label13.grid(row=6, column=0, pady=2)
            self.label14.grid(row=6, column=1, pady=2)
            self.label15.grid(row=7, column=0, pady=2)
            self.label16.grid(row=7, column=1, pady=2)
            self.frame4.place(anchor=tk.CENTER, relx=0.50, rely=0.50)
            self.label17.grid(column=0, row=0, columnspan=2, padx=2, pady=2)
            self.button8.grid(column=0, row=1, padx=2, pady=2)
            self.button9.grid(column=1, row=1, padx=2, pady=2)
        elif self.stage == 'game':
            self.frame.pack(fill=tk.BOTH, expand=tk.YES)
            self.canvas.place(y=0, x=0, bordermode="outside", width=self.root.content.winfo_width(
            ), height=self.root.winfo_height()-120, anchor=tk.NW)
            self.option1.canvas.place(y=self.root.winfo_height(), x=(self.root.content.winfo_width(
            )//6*1)-75, width=120, height=120, bordermode="outside", anchor=tk.S)
            self.option2.canvas.place(y=self.root.winfo_height(), x=(self.root.content.winfo_width(
            )//6*2)-75, width=120, height=120, bordermode="outside", anchor=tk.S)
            self.option3.canvas.place(y=self.root.winfo_height(), x=(self.root.content.winfo_width(
            )//6*3)-75, width=120, height=120, bordermode="outside", anchor=tk.S)
            self.option4.canvas.place(y=self.root.winfo_height(), x=(self.root.content.winfo_width(
            )//6*4)-75, width=120, height=120, bordermode="outside", anchor=tk.S)
            self.option5.canvas.place(y=self.root.winfo_height(), x=(self.root.content.winfo_width(
            )//6*5)-75, width=120, height=120, bordermode="outside", anchor=tk.S)
            self.option6.canvas.place(y=self.root.winfo_height(), x=(self.root.content.winfo_width(
            )//6*6)-75, width=120, height=120, bordermode="outside", anchor=tk.S)
        elif self.stage == 'end_game':
            self.frame.pack(fill=tk.BOTH, expand=tk.YES)
            self.canvas.place(y=0, x=0, bordermode="outside", width=self.root.content.winfo_width(
            ), height=self.root.winfo_height()-120, anchor=tk.NW)
            self.doneFrame.place(x=self.frame.winfo_width()//2, y=self.frame.winfo_height()//2,
                                 width=self.frame.winfo_width()//2, height=self.frame.winfo_height()//3, anchor=tk.CENTER)
            self.doneLabel.place(
                x=0, y=25, width=self.doneFrame.winfo_width(), height=50, anchor=tk.NW)
            self.doneButton.place(x=25, y=self.doneFrame.winfo_height(
            ) // 2, width=self.doneFrame.winfo_width()-50, height=50, anchor=tk.W)
            self.doneScore.place(x=0, y=self.doneFrame.winfo_height(
            )-25, width=self.doneFrame.winfo_width(), height=50, anchor=tk.SW)

        self.root.root.update()


if __name__ == '__main__':
    sim = Sim()
